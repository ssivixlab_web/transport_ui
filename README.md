# MycareWagon_UI

MycareWagon_UI Integration

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build:prod` to build the project for a production build.
Run `ng build:dev` to build the project for a Staging|Dev build.

## Running lint

Run `ng lint` to ensure code Quality standards are meet.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Known Issue in deployment

Folow the below steps:

1) In build folder search `firebase-messaging-sw.js` in all the files and you will get 2 hits in 2 files. Remove the forward slash `/` before `firebase-messaging-sw.js`
2) In the same 2 previously searched files find `firebase-cloud-messaging-push-scope`. Remove the forward slash `/` before `firebase-cloud-messaging-push-scope`
3) In index.html add href part in `/web/transport/` in production mainfest file and `/stage/transport/` in stage manifest file.
