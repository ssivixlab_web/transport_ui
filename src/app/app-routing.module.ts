import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppAuthGuardService as AuthGuard } from './common/utility/auth-guard.service';
import { TAPolicyComponent } from './taModules/policy/policy.component';
import { TATermsConditionComponent } from './taModules/terms-condition/terms-condition.component';
import { TAWorkSpaceComponent } from './taModules/workspace/workspace.component';
// import { ProfileCheckGuardService as profileCheck } from './common/utility/profileCheck-guard.service';
const TARoutes: Routes = [
  {
    path: '',
    data: {
      title: 'Login',
    },
    loadChildren: () => import('./taModules/home/home.module').then(module => module.TAHomeModule),
  },
  {
    path: 'ta',
    component: TAWorkSpaceComponent,
    children: [
      {
        path: 'book',
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        data: {
          title: 'Book A Medical Vehicle',
        },
        loadChildren: () => import('./taModules/booking/booking.module').then(m => m.BookingModule)
      },
      {
        path: 'profile',
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        data: {
          title: 'My Profile',
        },
        loadChildren: () => import('./taModules/profile/profile.module').then(m => m.TAProfileModule)
      },
      {
        path: 'history',
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        data: {
          title: 'My booking',
        },
        loadChildren: () => import('./taModules/history/history.module').then(m => m.HistoryModule)
      },
      {
        path: 'notification',
        canActivate: [AuthGuard],
        data: {
          title: 'My Notification',
        },
        loadChildren: () => import('./taModules/notification/notification.module').then(m => m.NotificationModule)
      },
      {
        path: 'payVerify',
        canActivate: [AuthGuard],
        data: {
          title: 'Payment Verification',
        },
        loadChildren: () => import('./taModules/payVerify/payVerify.module').then(m => m.PayVerifyModule)

      },
      {
        path: 'policy',
        data: {
          title: 'Our Policy',
        },
        component: TAPolicyComponent
      },
      {
        path: 'terms',
        data: {
          title: 'Our Terms & Conditions',
        },
        component: TATermsConditionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(TARoutes,
    {
      enableTracing: false,
      useHash: true
    })],
  exports: [RouterModule]
})
export class TARoutingModule { }
