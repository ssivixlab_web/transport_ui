import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'src/environments/environment';
import { TARoutingModule } from './app-routing.module';
import { TAComponent } from './app.component';
import { TACommonModule } from './common/common.module';
import { TAMessageNotificationService } from './common/message-notification/messageNotification.service';
import { AppHttpApiService } from './common/utility/http-api.service';
import { TAPolicyComponent } from './taModules/policy/policy.component';
import { TATermsConditionComponent } from './taModules/terms-condition/terms-condition.component';
import { TAWorkSpaceComponent } from './taModules/workspace/workspace.component';

@NgModule({
  declarations: [
    TAComponent,
    TATermsConditionComponent,
    TAPolicyComponent,
    TAWorkSpaceComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TACommonModule,
    TARoutingModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireModule.initializeApp(environment.firebaseConfig1),
  ],
  providers: [
    AppHttpApiService,
    TAMessageNotificationService,
  ],
  bootstrap: [TAComponent]
})
export class AppModule { }
