import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { SharedModule } from '../shared/shared.module';
import { AppMaterialModule } from './app-material.module';
import { ConfirmModalModule } from './confirmModal/confirmModal.module';
import { LoaderModule } from './loader/loader.module';
import { TAMessageNotificationComponent } from './message-notification/messageNotification.component';
import { AppPublishSubscribeService } from './pub-sub/publish-subscribe.service';
import { TAAppErrorService } from './utility/app-error.service';
import { AppAuthenticationService } from './utility/authentication.service';
import { AppInterceptService } from './utility/intercept.service';
import { MessagingService } from './utility/messaging.service';
import { AppNumberOnlyDirective } from './utility/number-only.directive';
import { SafePipe } from './utility/safe.pipe';
import { FormatTimePipe } from './utility/timeFormat.pipe';
import { Utils } from './utility/utility';

@NgModule({
  declarations: [
    SafePipe,
    FormatTimePipe,
    AppNumberOnlyDirective,
    TAMessageNotificationComponent,
  ],
  imports: [
    CommonModule,
    LoaderModule,
    ConfirmModalModule,
    AlertModule.forRoot()
  ],
  exports: [
    CommonModule,
    LoaderModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    SafePipe,
    FormatTimePipe,
    AppNumberOnlyDirective,
    TAMessageNotificationComponent,
  ],
  providers: [
    Utils,
    AppPublishSubscribeService,
    AppAuthenticationService,
    MessagingService,
    TAAppErrorService,
    // {
    //   provide: ErrorHandler,
    //   useClass: GlobalErrorHandler
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptService,
      multi: true
    },
  ]
})
export class TACommonModule { }
