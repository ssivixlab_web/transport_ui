import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmModalComponent } from './confirmModal.componet';

@NgModule({
    declarations: [ConfirmModalComponent],
    imports: [CommonModule,
        MatCardModule,
        MatDialogModule,
        MatButtonModule],
    exports: [ConfirmModalComponent],
    providers: [],
    entryComponents: [ConfirmModalComponent]
})

export class ConfirmModalModule { }
