import { environment } from 'src/environments/environment';

export const API_END_POINT_URL = environment.endPointUrl;
// All get/post/put/delete End point
export const API_URL = {
    LOGOUT: 'user/logOut',
    PHONELOGIN: 'phoneNumber',
    OTP_VERIFY: 'userOtpVerification',
    RESEND_OTP: 'user/resend/otp',
    USER_UPDATE: 'user/update/self/info',
    USER_PROFILE: 'fetch/user/profile',
    FAMILY_MEMBER: 'fetch/family/member/details',
    EDIT_USER: 'edit/userRelativeDetails',
    ADD_RELATIVE: 'add/relative',
    FETCH_BY_ID: 'fetch/family/member/details',
    UPLOAD_PROFILE_PIC: 'update/user/profile/pic',
    UPLOAD_RELATIVE_PIC: 'update/user/relative/profile/pic',
    BOOK: 'additional/item',
    FARE: 'fare',
    BOOK_STEP: 'additional/item',
    COMPLETED_TRIP: 'user/completed/trip',
    SCHEDULED_TRIP: 'user/scheduled/trip',
    WAGON_BOOK: 'book/vehicle',
    PAY_TRIP_REQUEST: 'payment/request',
    INVOICE: 'user/generate/invoice',
    EMAIL_UPDATE: 'user/email/update',
    CANCEL_TRIP: 'cancel/trip',
    PAYMENT_STATUS_VERIFY: 'todopayCheck'
};

export const STATIC_BASE_API_URL = {
    INSURANCE_PROVIDER: 'insurance/provider',
    HOSPITAL_LIST_FARE: 'fare/hospital',
    PRIVACY: 'https://ssivixlab.com/privacypolicy.htm',
    TERMSCONDITION: 'https://ssivixlab.com/termsandcondition.html'
};

// Session, Header, Localstorage related constants
export const SESSION_STORAGE_KEY = {
    TOKEN: 'SSIVIXLAB_TRANSPORT_authtoken',
    USER_DATA: 'SSIVIXLAB_TRANSPORT_user_data',
    OTHER_INFO: 'SSIVIXLAB_TRANSPORT_other_info',
    PAYMENT: 'SSIVIXLAB_TRANSPORT_pay_detail'
};

export const NO_INTERCEPT_LOADER = 'NO__LOADER';
export const placeholderImg = 'https://s3.us-east-2.amazonaws.com/smarthelpbucket/logos/patient_default.png';
export const NA = 'N/A';
export const SPACE = '';
export const SELF_PROFILE = 'self';
export const MAP_API_KEY = 'AIzaSyCZ57n0jGisxNWloY_F2GrjcjKEoGbjEZ4';
