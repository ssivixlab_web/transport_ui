import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class LoaderService {
    private isLoading$ = new Subject<boolean>();
    show() {
        this.isLoading$.next(true);
    }
    hide() {
        this.isLoading$.next(false);
    }
    getSpinnerStatus(): Observable<boolean> {
        return this.isLoading$;
    }
}
