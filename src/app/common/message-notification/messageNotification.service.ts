import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
export enum EmessageType {
    SUCCESS = 'success',
    ERROR = 'danger'
}

export interface ImessageData {
    type: EmessageType,
    data: string,
    timeout?: number
}

@Injectable({ providedIn: 'root' })
export class TAMessageNotificationService {
    private msg$ = new Subject<ImessageData[]>();
    private default_time: number = 4000;
    constructor() {
    }
    public setMsg(msgData: ImessageData[]) {
        msgData.forEach((it) => {
            if (!it.timeout) { it.timeout = this.default_time; }
        });
        this.msg$.next(msgData);
    }

    public getMsg(): Observable<ImessageData[]> {
        return this.msg$;
    }
}
