export interface IInsurnaceList {
    companyName: string;
    createdOn: string;
    deleted: boolean;
    _id: string;
}

export interface IKeyVal {
    key: string;
    val: string;
}
