import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmessageType, TAMessageNotificationService } from '../message-notification/messageNotification.service';

export type IERRORCODE = {
    [key in string | number]: string;
};
@Injectable()
export class TAAppErrorService {

    constructor(private errorMsg: TAMessageNotificationService) { }
    private errorcode: IERRORCODE = {
        409: 'Your trip has been already requested',
        417: 'Invalid referral code, please try another referral code',
        10002: 'Be patience with us. There is some technical glitch and we are working to resolve. Thanks for your understanding.',
        10008: 'Sorry wrong OTP or your OTP expired. Plrease click on resend OTP and try',
        10003: 'Please enter the missing field details.',
        10006: 'Sorry. Session got expired. Please re-login again',
        10021: 'One User Self profile already exists for this user',
        10024: 'Sorry, Maximum 6 profiles are only allowed per User'
    };

    private appError = {
        FCM: 'Please enable push notification to Continue.',
        CONTACT_SUPPORT: 'Looks Like thier is some problem, Kindly contact customer support.',
        ENABLE_LOCATION: 'Please enable the Location',
        PICKUP_LOC_REQUIRED: 'Please select pick up location',
        DROP_LOC_REQUIRED: 'Please select drop location',
        SAME_COUNTRY: 'Pick up and drop country should be same',
        DISTANCE_EXCEDS: 'Please select drop location within 100 kms',
        TRANSITION_CANCEL: 'The Transaction has been Cancelled'
    };

    showErrormessage(error) {
        let errorMsg: string = '';
        if (error.error_message) {
            errorMsg = error.error_message;
        } else {
            errorMsg = this.errorcode[error.error_code];
        }
        if (errorMsg !== undefined) {
            this.errorMsg.setMsg([{
                type: EmessageType.ERROR,
                data: errorMsg
            }]);
        }
    }

    showApplicationError(error: string) {
        const errorMsg: string = this.appError[error];
        if (errorMsg !== undefined) {
            this.errorMsg.setMsg([{
                type: EmessageType.ERROR,
                data: errorMsg
            }]);
        }
    }

    getClientMessage(error: Error): string {
        if (!navigator.onLine) {
            return 'No Internet Connection';
        }
        return error.message ? error.message : error.toString();
    }

    getClientStack(error: Error): string {
        return error.stack;
    }

    getServerMessage(error: HttpErrorResponse): string {
        return error.message;
    }

    getServerStack(error: HttpErrorResponse): string {
        // handle stack trace
        return 'stack';
    }

    showSuccessMessage(message: string) {
        this.errorMsg.setMsg([{
            type: EmessageType.SUCCESS,
            data: message
        }]);
    }
}
