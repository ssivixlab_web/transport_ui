
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { TAAppErrorService } from './app-error.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private injector: Injector) { }
  handleError(error: Error | HttpErrorResponse) {
    const notifier = this.injector.get(TAAppErrorService);
    if (error instanceof HttpErrorResponse) {
      // Server Error
    } else {
      // Client Error
      let message = notifier.getClientMessage(error);
      let stackTrace = notifier.getClientStack(error);
      if (message !== 'google is not defined') {
        notifier.showApplicationError('CONTACT_SUPPORT');
      }
    }
  }
}
