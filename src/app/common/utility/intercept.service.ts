import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SESSION_STORAGE_KEY } from '../contants';
import { TAAppErrorService } from './app-error.service';
import { AppAuthenticationService } from './authentication.service';

@Injectable()
export class AppInterceptService implements HttpInterceptor {
    sessionID: string = '';
    constructor(private appError: TAAppErrorService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.sessionID === '') {
            this.sessionID = AppAuthenticationService.getByKey(SESSION_STORAGE_KEY.TOKEN);
        }
        if (this.sessionID && this.sessionID !== '') {
            request = request.clone({
                setHeaders: {
                    authToken: `${this.sessionID}`
                }
            });
        }
        return next.handle(request)
            .pipe(
                map((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        if (event.body.error_code !== '' && event.body.error_code !== undefined) {
                            this.appError.showErrormessage(event.body);
                            return null;
                        }
                    }
                    return event;
                }),
                catchError((error: HttpErrorResponse | any) => {
                    // console.log('error--->>>', error);
                    if (!error.error.status && error.error.error_code !== '') {
                        this.appError.showErrormessage(error.error);
                    }
                    return throwError(error);
                })
            );
    }
}
