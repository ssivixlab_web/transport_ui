import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MessagingService {

  currentMessage$ = new BehaviorSubject(null);
  token$ = new BehaviorSubject(null);
  private fcmToken: string;

  constructor(
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (messaging) => {
        messaging.onMessage = messaging.onMessage.bind(messaging);
        messaging.onTokenRefresh = messaging.onTokenRefresh.bind(messaging);
      }
    );
  }

  /**
   * request permission for notification from firebase cloud messaging
   */
  private requestToken() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        this.fcmToken = token;
        console.log('messaging', token)
        this.token$.next(token);
      },
      (err) => {
        console.log('messaging', err)
      }
    );
  }

  public requestPermission() {
    Notification.requestPermission((result) => {
      this.getFcmToken();
      if (result === 'denied') {
        // console.log('Permission wasn\'t granted. Allow a retry.');
        return;
      } else if (result === 'default') {
        // console.log('The permission request was dismissed.');
        return;
      }
    });
  }

  getFcmToken() {
    if (this.fcmToken == null || this.fcmToken === '') {
      this.requestToken();
    }
    return this.fcmToken;
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        this.currentMessage$.next(payload);
      });
  }
}
