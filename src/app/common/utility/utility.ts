import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
export interface UserCurrentLoc {
    cords: Coordinates,
    results?: google.maps.GeocoderResult[]
}
@Injectable()
export class Utils {

    static getLocation(): Observable<UserCurrentLoc> {
        return new Observable(observer => {
            if (window.navigator && window.navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (position: Position) => {
                        let res: UserCurrentLoc = null;
                        const cords = position.coords;
                        res = { ...res, cords };
                        observer.next(res);
                        const geocoder = new google.maps.Geocoder();
                        let geoReq: google.maps.GeocoderRequest = {
                            location: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
                        };
                        geocoder.geocode(geoReq, (results, status) => {
                            if (status === google.maps.GeocoderStatus.OK) {
                                res = { ...res, results };
                                observer.next(res);
                            }
                            observer.complete();
                        });
                    }, (error) => {
                        observer.error(error);
                    }, {
                    enableHighAccuracy: true,
                    maximumAge: 60000,
                    // timeout: 2000
                }
                );
            } else {
                observer.error('Unsupported Browser');
            }
        });
    }

    static getGeoReult(location): Observable<google.maps.GeocoderResult[]> {
        return new Observable(observer => {
            const geocoder = new google.maps.Geocoder();
            let geoReq: google.maps.GeocoderRequest = {
                location: new google.maps.LatLng(location.lat, location.lng)
            };
            geocoder.geocode(geoReq, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                    observer.next(results);
                }
                observer.complete();
            });
        });
    }

    static getAgeFromDateOfBirth(dateOfBirth) {
        const yearOfBirth = parseInt(moment(dateOfBirth).format('YYYY'), 10);
        const yearToday = parseInt(moment(new Date()).format('YYYY'), 10);
        return yearToday - yearOfBirth;
    }

    static getAutoCompleteOption(cords: Coordinates): google.maps.places.AutocompleteOptions {
        return {
            types: ['address'],
            bounds: new google.maps.LatLngBounds(new google.maps.LatLng(
                {
                    lat: cords.latitude,
                    lng: cords.longitude
                })),
        };
    }

    static isTimeDiffMoreThenThreeHours(bookingTime) {
        const timeFormat = 'YYYY-MM-DDTHH:mm:ss a';
        const days = 0;
        const endTimeString = moment(bookingTime).local().format(timeFormat);
        const todayString = moment(new Date()).format(timeFormat);
        const startTime = moment(todayString, timeFormat);
        const endTime = moment(endTimeString, timeFormat);
        const duration = moment.duration(endTime.diff(startTime));
        const hours = duration.asHours().valueOf();
        const minutes = duration.asMinutes().valueOf() - hours * 60;
        const seconds = (duration.asMilliseconds().valueOf() / 1000) % 60;

        if (hours === 3 && minutes === 0) {
            return true;
        } else if (hours < 3) {
            return true;
        } else {
            return false;
        }
    }
}
