import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { API_URL } from 'src/app/common/contants';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public user$: Observable<any>;
  public emails$: Observable<any[]>;
  public pageInfo: any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private httpApi: AppHttpApiService
  ) {
  }

  public ngOnInit() {
    this.pageTitle();
    if (new RegExp(/^\/ta/).test(this.router.url)) {
      const urls = this.router.url.split('/');
      const pathQueyParam = urls[2].split('?');
      const pathToMatch = pathQueyParam[0];
      let temp = this.activatedRoute.routeConfig.children.filter(ev => ev.path === pathToMatch);
      this.pageInfo = temp[0].data;
      this.titleService.setTitle(`MedicalTransport :: ${this.pageInfo.title}`);
    }
  }

  public openMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;

    this.isShowSidebar.emit(this.isMenuOpened);
  }

  public signOut(): void {
    this.httpApi.postData(API_URL.LOGOUT, {}).subscribe((res) => {
      AppAuthenticationService.removeSessionStorage();
      this.router.navigateByUrl('');
    });
  }

  public pageTitle() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        })
      )
      .pipe(filter(route => route.outlet === 'primary'))
      .pipe(mergeMap(route => route.data))
      .subscribe(event => {
        this.titleService.setTitle(`MedicalTransport :: ${event.title}`);
        this.pageInfo = event;
      });
  }
}
