import { Injectable } from '@angular/core';
import { ISideNav } from './sidebar';

const MENUITEMS = [
  { path: '/ta/book', title: 'Book', icon: 'local_taxi' },
  { path: '/ta/history', title: 'My Bookings', icon: 'book_online' },
  { path: '/ta/profile', title: 'Profile', icon: 'contacts' },
  { path: '/ta/notification', title: 'Notification', icon: 'notifications' },
  { path: '/ta/policy', title: 'Privacy Policy', icon: 'privacy_tip' },
  { path: '/ta/terms', title: 'Terms & Conditions', icon: 'list_alt' }
];

@Injectable()
export class SideNavService {
  getMenuitem(): ISideNav[] {
    return MENUITEMS;
  }
}
