import { MapsAPILoader } from '@agm/core';
import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { confirmDialog, confirmDialogAction, ConfirmModalComponent } from 'src/app/common/confirmModal/confirmModal.componet';
import { TAAppErrorService } from 'src/app/common/utility/app-error.service';
import { UserCurrentLoc, Utils } from 'src/app/common/utility/utility';
import { TABookingService } from './booking.service';
import { BookingModalComponent } from './modal/booking.modal.component';
import { HospitalModalComponent } from './modal/hospital.modal.component';
import { ReturnTimeModalComponent } from './modal/returnTime.modal.component';

@Component({
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  staticItemList: any;

  userCurrentLocation: UserCurrentLoc = null;
  paymentMethod: FormControl = new FormControl('CASH');
  unitNumber: FormControl = new FormControl('');
  returnTime: FormControl = new FormControl(null);
  bookForm: FormGroup;
  fare: string;
  private modalConfig: MatDialogConfig = {
    closeOnNavigation: true,
    disableClose: true
  };
  @ViewChild('datePicker') public datePicker: MatDatepicker<any>;
  @ViewChild('pickupUI') pickupUI: any;
  @ViewChild('dropUI') dropUI: any;
  private _setPickUpGoogleObj = null;
  private _setDropGoogleObj = null;
  minDate: Date; maxDate: Date;

  private hospitalList: any;
  constructor(private bookService: TABookingService,
    public dialog: MatDialog,
    private _fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private renderer: Renderer2,
    private errMsg: TAAppErrorService) { }

  ngOnInit() {
    this.staticItemList = this.bookService.getStaticItemList();
    this.createForm();
    this._setMinMaxDate();
    this.getFare();
    this.mapsAPILoader.load().then(() => {
      this.setCurrentPosition();
    });
  }

  get _isDrop() { return this.bookForm.get('dropLocation').value !== '' ? true : false; }
  setDrop(value?: string) { this.bookForm.get('dropLocation').setValue(value ? value : ''); }

  get _isPickUp() { return this.bookForm.get('pickupLocation').value !== '' ? true : false; }
  setPickUp(value?: string) { this.bookForm.get('pickupLocation').setValue(value ? value : ''); }

  private createForm() {
    this._setPickUpGoogleObj = null;
    this._setDropGoogleObj = null;
    this.bookForm = this._fb.group({
      tripType: ['SINGLE'],
      bookingType: ['BOOK_NOW'],
      vehicleType: ['AMBULANCE'],
      bookingTime: [new Date()],
      pickupLocation: ['', Validators.required],
      dropLocation: ['', Validators.required]
    });
  }
  private _setMinMaxDate() {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.maxDate.setDate(new Date().getDate() + 100);
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      Utils.getLocation().subscribe((res: UserCurrentLoc) => {
        this.userCurrentLocation = res;
        this.bindPickUP();
        this.bindDrop();
        this.setCurrentLocation();
      });
    }
  }

  private createBussinessData(googleObj: google.maps.GeocoderAddressComponent[]) {
    const countryName = googleObj.filter((val) => {
      return val.types.indexOf('country') > -1;
    });

    const stateName = googleObj.filter((val) => {
      return val.types.indexOf('administrative_area_level_1') > -1;
    });

    return {
      country: countryName && countryName.length > 0 ? countryName[0] : null,
      state: stateName && stateName.length > 0 ? stateName[0] : null
    };
  }

  private bindPickUP() {
    const pickuAutoComplete = new google.maps.places.Autocomplete(
      this.pickupUI.nativeElement,
      Utils.getAutoCompleteOption(this.userCurrentLocation.cords)
    );
    google.maps.event.addListener(pickuAutoComplete, 'place_changed', () => {
      this.setPickUp(pickuAutoComplete.getPlace().formatted_address);
      this._setPickUpGoogleObj = this.createBussinessData(pickuAutoComplete.getPlace().address_components);
    });
  }

  private bindDrop() {
    const dropAutoComplete = new google.maps.places.Autocomplete(
      this.dropUI.nativeElement,
      Utils.getAutoCompleteOption(this.userCurrentLocation.cords)
    );
    google.maps.event.addListener(dropAutoComplete, 'place_changed', () => {
      this.setDrop(dropAutoComplete.getPlace().formatted_address);
      this._setDropGoogleObj = this.createBussinessData(dropAutoComplete.getPlace().address_components);
    });
  }

  setCurrentLocation() {
    if (this.userCurrentLocation) {
      if (this.userCurrentLocation.results) {
        const userCurrenLoc: google.maps.GeocoderResult = this.userCurrentLocation.results[0];
        this._setPickUpGoogleObj = this.createBussinessData(userCurrenLoc.address_components);
        this.setPickUp(userCurrenLoc.formatted_address);
      }
    } else {
      this.errMsg.showApplicationError('ENABLE_LOCATION');
    }
  }

  getHospitalList() {
    const trip = {
      tripType: this.bookForm.get('tripType').value,
      vehicleType: this.bookForm.get('vehicleType').value
    };
    this.bookService.getHospital(trip).subscribe((res) => {
      this.hospitalList = res;
      this.calculateFare(res.fare);
      this.openHospitalListModal();
    });
  }

  private calculateFare(fare) {
    if (fare) {
      if (fare.minFare === fare.maxFare) {
        this.fare = fare.minFare;
      } else {
        this.fare = `${fare.minFare} - ${fare.maxFare}`;
      }
    } else {
      this.fare = '-';
    }
  }

  private getFare() {
    const trip = {
      tripType: this.bookForm.get('tripType').value,
      vehicleType: this.bookForm.get('vehicleType').value
    };
    this.bookService.getFare(trip).subscribe((res) => {
      this.calculateFare(res.data);
    });
  }

  private openHospitalListModal() {
    const dialogRef = this.dialog.open(
      HospitalModalComponent, { ...this.modalConfig, data: this.hospitalList }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.findCountry(result);
      }
    });
  }

  private findCountry(res) {
    this.setDrop(res.description);
    Utils.getGeoReult(res.geometry.location).subscribe((result: google.maps.GeocoderResult[]) => {
      this._setDropGoogleObj = this.createBussinessData(result[0].address_components);
    });
  }

  private bookingDialog(bookDialogData) {
    const dialogRef = this.dialog.open(
      BookingModalComponent, { ...this.modalConfig, data: bookDialogData }
    );
    dialogRef.afterClosed().subscribe(result => {
      ////if (result) {
      //  this._setDropGoogleObj = result;
      //}
    });
  }

  private validateBookingData(): Observable<boolean> {
    return new Observable(observer => {
      let validationPass: boolean = true;
      if (this.bookForm.get('pickupLocation').value === '') {
        validationPass = false;
        this.errMsg.showApplicationError('PICKUP_LOC_REQUIRED');
      }
      if (this.bookForm.get('dropLocation').value === '') {
        validationPass = false;
        this.errMsg.showApplicationError('DROP_LOC_REQUIRED');
      }
      if (this._setPickUpGoogleObj && this._setDropGoogleObj &&
        this._setPickUpGoogleObj.country.long_name !== this._setDropGoogleObj.country.long_name) {
        validationPass = false;
        this.errMsg.showApplicationError('SAME_COUNTRY');
      }
      // Distance Validate
      if (validationPass && this.bookForm.get('dropLocation').value !== '' && this.bookForm.get('dropLocation').value !== '') {
        const loc = {
          pickup: this.bookForm.get('pickupLocation').value,
          drop: this.bookForm.get('dropLocation').value
        };
        this.bookService.getDistanceBetweenPickAndDropLocation(loc).subscribe((res) => {
          if (res > 100000) {
            validationPass = false;
            this.errMsg.showApplicationError('DISTANCE_EXCEDS');
          }
          observer.next(validationPass);
          observer.complete();
        }, (err) => {
          observer.error(err);
          console.log('Unable to Calucate Distance');
        }, () => {
          observer.complete();
        });
      }
    });

  }
  /**
   * This is called after callender is set -- Book Later
   * Book Now
   */
  continueWithBooking() {
    this.validateBookingData().subscribe((res) => {
      if (res) {
        let payload = { ...this.bookForm.value };
        payload.bookingTime = moment(payload.bookingTime, 'YYYY-MM-DD').toDate().toISOString();
        this.book(payload);
      }
    });
  }

  book(payload) {
    let returnTime = (this.returnTime.value !== 'NOT_SURE') ?
      moment(payload.bookingTime).add(parseInt(this.returnTime.value, 10), 'hours') :
      moment(payload.bookingTime).add(parseInt('1', 10), 'hours');

    this.bookService.booking(payload).subscribe((response) => {
      let bookData = {
        ...payload,
        fare: response.fare,
        additionalItem: response.additionalItem,
        country: this._setPickUpGoogleObj.country.long_name,
        unitNumber: this.unitNumber.value,
        paymentMethod: this.paymentMethod.value,
        returnTime: (payload.tripType === 'SINGLE') ? null : returnTime
      };
      this.bookingDialog(bookData);
    });
  }

  bookingTypeChange(value: string) {
    if (value === 'BOOK_NOW') {
      this.bookForm.get('bookingTime').setValue(new Date());
    }
  }

  openCalender() {
    this.datePicker.open();
    // Adding a click event to button....
    setTimeout(() => {
      this.renderer.listen(document.querySelector('.actions button'), 'click', (event) => {
        this.continueWithBooking();
      });
    }, 1);
  }

  private openConfirmation() {
    let confirmDi: confirmDialog = {
      dialogTitle: 'Wheelchair Fleet',
      dialogContent: 'No paramedic and other facility is available. Are you ok to Proceed ?',
      dialogPrimaryBtnText: 'Yes',
      dialogSecondaryBtnText: 'No',
      dialogFeatureName: 'VEICHLE_CHANGE_CONFIRM'
    };
    const dialogRef = this.dialog.open(
      ConfirmModalComponent, { ...this.modalConfig, data: confirmDi });
    dialogRef.afterClosed().subscribe((result: confirmDialogAction) => {
      if (result.dialogFeatureName === 'VEICHLE_CHANGE_CONFIRM' && !result.btnConfirm) {
        this.bookForm.get('vehicleType').setValue('AMBULANCE');
      }
      this.getFare();
    });
  }

  private openReturnTimeModal() {
    const dialogRef = this.dialog.open(
      ReturnTimeModalComponent, { ...this.modalConfig });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.returnTime.setValue(result);
      }
    });
  }

  vechileTypeChange(value: string) {
    if (value === 'OTHER_VEHICLE') {
      this.openConfirmation();
    } else {
      this.getFare();
    }
  }

  tripTypeChange(value: string) {
    if (value === 'ROUND') {
      this.openReturnTimeModal();
    }
    this.getFare();
  }
}
