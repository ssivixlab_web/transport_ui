import { AgmCoreModule } from '@agm/core';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';
import { Platform } from '@angular/cdk/platform';
import { NgModule } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { RouterModule, Routes } from '@angular/router';
import { TACommonModule } from 'src/app/common/common.module';
import { MAP_API_KEY } from 'src/app/common/contants';
import { CustomDateAdapter } from 'src/app/common/customDate';
import { BookingComponent } from './booking.component';
import { TABookingService } from './booking.service';
import { BookingModalComponent } from './modal/booking.modal.component';
import { HospitalModalComponent } from './modal/hospital.modal.component';
import { ReturnTimeModalComponent } from './modal/returnTime.modal.component';
const bookingRoutes: Routes = [
  { path: '', component: BookingComponent }
];

let Modalcomponets = [
  HospitalModalComponent,
  BookingModalComponent,
  ReturnTimeModalComponent
];

@NgModule({
  declarations: [BookingComponent, ...Modalcomponets],
  imports: [
    TACommonModule,
    RouterModule.forChild(bookingRoutes),
    NgxMatDatetimePickerModule,
    NgxMatNativeDateModule,
    NgxMatTimepickerModule,
    AgmCoreModule.forRoot({
      apiKey: MAP_API_KEY,
      libraries: ['places']
    })
  ],
  providers: [
    TABookingService,
    {
      provide: DateAdapter,
      useClass: CustomDateAdapter,
      deps: [MAT_DATE_LOCALE, Platform]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'DD-MM-YYYY',
        },
        display: {
          dateInput: 'DD-MM-YYYY',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM-YYYY',
        }
      }
    }
  ],
  entryComponents: [...Modalcomponets]
})
export class BookingModule { }
