import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { API_URL, NO_INTERCEPT_LOADER, STATIC_BASE_API_URL } from 'src/app/common/contants';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable()
export class TABookingService {
    private _cacheItems: Observable<Array<Observable<any>>> = new Observable();
    constructor(private httpApi: AppHttpApiService) {
    }

    public getStaticItemList() {
        return {
            vehicleTypeList: [{ key: 'AMBULANCE', val: 'Medical Transport' }, { key: 'OTHER_VEHICLE', val: 'Wheelchair fleet' }],
            tripTypeList: [{ key: 'SINGLE', val: 'One Way' }, { key: 'ROUND', val: 'Two Way' }],
            payTypeList: [{ key: 'CASH', val: 'Cash' }, { key: 'CARD', val: 'Card' }],
            returnTimeList: [{ key: 1, val: '1 Hour' }, { key: 2, val: '2 Hour' }, { key: 3, val: '3 Hour' }, { key: 4, val: '4 Hour' },
            { key: 5, val: '5 Hour' }, { key: 6, val: '6 Hour' }, { key: 7, val: '7 Hour' }, { key: 8, val: '8 Hour' },
            { key: 9, val: '9 Hour' }, { key: 10, val: '10 Hour' }, { key: 'NOT_SURE', val: 'Not Sure' }]
        };
    }

    public getValueByKey(list: string, keyToFilter: string): string {
        let filtered = this.getStaticItemList()[list].filter((k) => k.key === keyToFilter);
        return filtered && filtered.length > 0 ? filtered[0].val : '';
    }

    /**
     * Get List of Hospital
     */
    public getHospital(payload): Observable<any> {
        if (!(this._cacheItems && this._cacheItems[`${STATIC_BASE_API_URL.HOSPITAL_LIST_FARE}`])) {
            this._cacheItems[`${STATIC_BASE_API_URL.HOSPITAL_LIST_FARE}`] =
                this.httpApi.postData(`${STATIC_BASE_API_URL.HOSPITAL_LIST_FARE}`, payload)
                    .pipe(
                        map((res) => {
                            return res.data;
                        }),
                        publishReplay(1),
                        refCount()
                    );
        }
        return this._cacheItems[`${STATIC_BASE_API_URL.HOSPITAL_LIST_FARE}`];
    }

    public getFare(payload): Observable<any> {
        let HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        let headers: HttpHeaders = new HttpHeaders();
        HttpUploadOptions.headers = headers.append(NO_INTERCEPT_LOADER, '');
        return this.httpApi.postData(`${API_URL.FARE}`, payload, HttpUploadOptions.headers);
    }

    public booking(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.BOOK_STEP}`, payload);
    }

    public getDistanceBetweenPickAndDropLocation(loc): Observable<number> {
        let distanceRequest: google.maps.DistanceMatrixRequest = {
            origins: [loc.pickup],
            destinations: [loc.drop],
            travelMode: google.maps.TravelMode.DRIVING
        };
        return new Observable(observer => {
            new google.maps.DistanceMatrixService().getDistanceMatrix(distanceRequest,
                (res: google.maps.DistanceMatrixResponse, status: google.maps.DistanceMatrixStatus) => {
                    if (status === google.maps.DistanceMatrixStatus.OK) {
                        let distance = res.rows[0].elements[0].distance.value;
                        observer.next(distance);
                    } else {
                        observer.next(-1);
                    }
                    observer.complete();
                });
        });
    }

    public bookWagon(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.WAGON_BOOK}`, payload);
    }
}
