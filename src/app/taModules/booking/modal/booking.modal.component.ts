import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { timer } from 'rxjs';
import { MessagingService } from 'src/app/common/utility/messaging.service';
import { TABookingService } from '../booking.service';
@Component({
    template: `
    <div *ngIf="!isSearchVechile">
        <h2 mat-dialog-title class="font-weight-light">Booking Details</h2>
        <mat-dialog-content class="bgWrapp">
        <div class='bgImg'></div>
        <form class="bringfront">
            <div class="d-flex mb-3">
                <div class="leftSection">Date</div>
                <div class="valueSection">{{dateTimeFormat(data.bookingTime)}}</div>
            </div>

            <div class="d-flex mb-3" *ngIf="data.tripType === 'ROUND'">
                <div class="leftSection">Return Time</div>
                <div class="valueSection">{{dateTimeFormat(data.returnTime)}}</div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Pickup Location</div>
                <div class="valueSection">{{data.pickupLocation}}</div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Drop Location</div>
                <div class="valueSection">{{data.dropLocation}}</div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Unit No</div>
                <div class="valueSection">{{data.unitNumber ? data.unitNumber : 'NA' }}</div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Vehicle</div>
                <div class="valueSection"> {{ getVal('vehicleTypeList', data.vehicleType) }} </div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Trip</div>
                <div class="valueSection"> {{ getVal('tripTypeList', data.tripType) }} </div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Fare</div>
                <div class="valueSection"> {{data.fare}} <b>(Estimated)</b> </div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Payment By</div>
                <div class="valueSection"> {{ getVal('payTypeList', data.paymentMethod) }} </div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Additional items</div>
                <div class="valueSection">
                    <span class="mr-3" *ngFor="let item of data.additionalItem">
                        <mat-checkbox [(ngModel)]="item.itemUsed" [ngModelOptions]="{standalone: true}"
                        [disabled]="data.vehicleType === 'OTHER_VEHICLE'"
                        (change)="calculateFare(item)">{{item.itemName}}</mat-checkbox>
                    </span>
                    <p class="text-muted font-italic mb-0" *ngIf="data.vehicleType === 'OTHER_VEHICLE'">
                        Sorry these options only applicable with Medical Transport
                    </p>
                </div>
            </div>

            <div class="d-flex mb-3">
                <div class="leftSection">Note</div>
                <div class="valueSection">
                <mat-form-field appearance="outline">
                    <mat-label>Add a special note</mat-label>
                        <textarea rows="2" matInput [formControl]="specialNotes"></textarea>
                    </mat-form-field>
                </div>
            </div>
        </form>
        </mat-dialog-content>
        <mat-dialog-actions align="end">
            <button mat-raised-button mat-dialog-close>Back</button>
            <button mat-raised-button color="accent" (click)="confirmBooking()">Confirm</button>
        </mat-dialog-actions>
    </div>
    <div *ngIf="isSearchVechile">
        <mat-dialog-content class="bgWrapp">
            <div class='bgImg'></div>
            <h2 mat-dialog-title class="font-weight-light text-center">Searching</h2>
            <div class="d-flex">
                <div class="col-6"><img class="img-fluid" src="../assets/images/map_design.png" /></div>
                <div class="col-6">
                    Will update you with in <br/><b>{{counter | formatTime}}</b>
                    <mat-progress-bar mode="indeterminate" color="accent"></mat-progress-bar>
                </div>
            </div>
        </mat-dialog-content>
    </div>
    `,
    styles: [`
        .bgWrapp{
            position: relative;
        }
        .bringfront {
            z-index:1;
            position: relative;
        }
        .bgImg{
            background-image: url('../assets/images/transport-img.png');
            width: 100%;
            height: 100%;
            position: absolute;
            background-repeat: no-repeat;
            background-position: center;
            opacity: 0.2;
            left: 0;
            top: 0;
        }
        .valueSection {
            width:100%;
        }
        .leftSection {
            width: 140px;
            flex: 0 0 140px;
            position: relative;
            color: #333;
            font-weight: 500;
        }
        .leftSection::after {
            content: " : ";
            position: absolute;
            right: 10px;
        }
    `]

})
export class BookingModalComponent implements OnInit {
    specialNotes: FormControl = new FormControl(null);
    counter = 900;
    isSearchVechile = false;
    constructor(
        public dialogRef: MatDialogRef<BookingModalComponent>,
        private messagingService: MessagingService,
        private bookService: TABookingService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private router: Router) {
    }

    ngOnInit() {
        this.messagingService.receiveMessage();
        this.messagingService.currentMessage$.subscribe((notificationRes) => {
            console.log('notificationRes', notificationRes)
            if (notificationRes && notificationRes.data.notify_about === 'AMBULANCE_BOOKED_SUCCESSFUL') {
                this.router.navigateByUrl('ta/history');
                this.dialogRef.close();
            }
        });
    }

    dateTimeFormat(bookingTime: string): string {
        if (bookingTime === 'NOT_SURE') {
            return 'Not Sure';
        } else {
            return moment(bookingTime).format('MMM Do, YYYY hh:mm A');
        }
    }

    getVal(listName: string, keyName: string): string {
        return this.bookService.getValueByKey(listName, keyName);
    }

    calculateFare(item) {
        let fare = parseInt(this.data.fare.substring(2), 10);
        if (item.itemUsed === item.itemUsedStatus) {
            fare = fare - parseInt(item.price, 10);
        } else {
            fare = fare + parseInt(item.price, 10);
        }
        this.data.fare = `S$${fare}`;
    }

    confirmBooking() {
        let bookingData = {
            ...this.data,
            specialNotes: this.specialNotes.value
        };
        this.bookService.bookWagon(bookingData).subscribe((res) => {
            if (res.status) {
                this.isSearchVechile = true;
                this.startTimer();
            }
        });
    }

    private startTimer() {
        timer(0, 1000).subscribe(() => {
            if (this.counter > 0) {
                this.counter--;
            }
        });
    }

}
