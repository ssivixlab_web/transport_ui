import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    template: `
    <h2 class="pl-3 mb-0" mat-dialog-title>Select Hospital</h2>
    <mat-dialog-content>
        <mat-nav-list>
            <a *ngFor="let item of data.hospital"  mat-list-item (click)="holspilatselected(item)">
            <span mat-line>{{item.hospitalName}}</span>
            <span mat-line>{{item.address}}</span>
            <mat-divider></mat-divider>
            </a>
        </mat-nav-list>
    </mat-dialog-content>
    `,
})
export class HospitalModalComponent implements OnInit {
    constructor(public dialogRef: MatDialogRef<HospitalModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
    }

    holspilatselected(item) {
        this.dialogRef.close(item);
    }
}
