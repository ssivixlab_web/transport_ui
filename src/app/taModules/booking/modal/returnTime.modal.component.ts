import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TABookingService } from '../booking.service';

@Component({
    template: `
    <h2 class="mb-0" mat-dialog-title>Select Return Time</h2>
    <mat-dialog-content class="pt-2">
        <mat-radio-group class="d-flex flex-column" aria-label="Select an Return Time option" (change)="clickEvent($event.value)">
            <mat-radio-button class="mb-2" *ngFor="let item of staticItemList.returnTimeList" [value]="item.key">{{item.val}}</mat-radio-button>
        </mat-radio-group>
    </mat-dialog-content>
    `,
})
export class ReturnTimeModalComponent {
    staticItemList: any;
    constructor(
        private dialogRef: MatDialogRef<ReturnTimeModalComponent>,
        private bookService: TABookingService) {
        this.staticItemList = this.bookService.getStaticItemList();
    }

    clickEvent(proceed: string) {
        this.dialogRef.close(proceed);
    }
}
