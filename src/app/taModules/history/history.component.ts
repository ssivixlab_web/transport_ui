import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { confirmDialog, confirmDialogAction, ConfirmModalComponent } from 'src/app/common/confirmModal/confirmModal.componet';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { TAAppErrorService } from 'src/app/common/utility/app-error.service';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { Utils } from 'src/app/common/utility/utility';
import { TAHistoryService } from './history.service';
import { BookingDetailModalComponent } from './modal/booking-detail.modal.component';
import { InvoiceModalComponent } from './modal/Invoice.modal.component';

@Component({
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  bookingType: FormControl = new FormControl('Upcoming');
  bookingData: any = [];
  emptypayload = { page: 1 };
  private modalConfig: MatDialogConfig = {
    closeOnNavigation: true,
    disableClose: true
  };

  constructor(
    private history: TAHistoryService,
    private dialog: MatDialog,
    private errMsg: TAAppErrorService,
    private route: ActivatedRoute) { }
  ngOnInit() {
    this.getScheduledTrip();
    this.route.queryParams.subscribe(params => {
      if (params && params.status) {
        this.errMsg.showApplicationError('TRANSITION_CANCEL');
        sessionStorage.removeItem(SESSION_STORAGE_KEY.PAYMENT);
      }
    });
  }

  getScheduledTrip() {
    this.history.scheduledTrip(this.emptypayload).subscribe((res) => {
      this.bookingData = res.data;
    });
  }

  getPastTrip() {
    this.history.completedTrip(this.emptypayload).subscribe((res) => {
      this.bookingData = res.data;
    });
  }

  formatTime(data) {
    return moment(data).format('MMM Do, YYYY hh:mm A');
  }

  paymentStatus(item) {
    return (item.paymentMethod === 'CARD') ?
      (item.paymentStatus === 'PENDING') ? 'Pending'
        : (item.paymentStatus === 'successful') ? 'Paid' : item.paymentStatus
      : item.paymentStatus;

  }

  paymentType(item) {
    return (item.paymentMethod === 'CARD') ?
      (item.paymentStatus === 'successful') ? 'CARD' : 'PAY'
      : 'CASH';
  }

  formatAmount(fare) {
    return `S$ ${fare.substring(2)}`;
  }

  openBookingDetail(bookDialogData, modal: string) {
    if (modal === 'detail') {
      const dialogRef = this.dialog.open(
        BookingDetailModalComponent, { ...this.modalConfig, data: bookDialogData });
      dialogRef.afterClosed().subscribe(result => {

      });
    }

  }

  makePayment(id) {
    this.history.paymentTrip({ bookingId: id }).subscribe((res) => {
      if (res.response_msg === 'successful') {
        const payVerify = {
          request_mid: res.mid,
          transaction_id: res.transaction_id,
          signature: res.redirectSignature
        };
        sessionStorage.setItem(SESSION_STORAGE_KEY.PAYMENT, JSON.stringify(payVerify));
        window.location.href = res.payment_url;
      }
    });
  }

  getInvoice(item) {
    let invoiceBody = {
      bookingId: item._id,
      email: AppAuthenticationService.getUserInfo().email
    };
    const dialogRef = this.dialog.open(
      InvoiceModalComponent, { ...this.modalConfig, data: invoiceBody });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.send) {
        this.mailInvoice(result.data);
      }
    });
  }

  private mailInvoice(mailInvoicePayload) {
    this.history.mailInvoice(mailInvoicePayload).subscribe((res) => {
      if (res.status && res.data.status) {
        this.invoiceMailessuccessCb(mailInvoicePayload);
      }
    });
  }

  private invoiceMailessuccessCb(mailInvoicePayload) {
    if (AppAuthenticationService.getUserInfo().email === '') {
      let confirmDi: confirmDialog = {
        dialogTitle: 'Invoice Sent',
        dialogContent: 'Invoice has been send to your mail. Do you want to add this email id for future use.',
        dialogPrimaryBtnText: 'Yes',
        dialogSecondaryBtnText: 'No',
        dialogFeatureName: 'INVOICE_EMAIL_CONFIRM'
      };
      const dialogRef = this.dialog.open(
        ConfirmModalComponent, { ...this.modalConfig, data: confirmDi });
      dialogRef.afterClosed().subscribe((result: confirmDialogAction) => {
        if (result.dialogFeatureName === 'INVOICE_EMAIL_CONFIRM' && result.btnConfirm) {
          this.updateEmail(mailInvoicePayload);
        }
      });
    } else if (AppAuthenticationService.getUserInfo().email !== mailInvoicePayload.email) {
      let confirmDi: confirmDialog = {
        dialogTitle: 'Invoice Sent',
        dialogContent: 'Invoice has been send to your mail. Do you want to update your email id.',
        dialogPrimaryBtnText: 'Yes',
        dialogSecondaryBtnText: 'No',
        dialogFeatureName: 'INVOICE_EMAIL_CONFIRM'
      };
      const dialogRef = this.dialog.open(
        ConfirmModalComponent, { ...this.modalConfig, data: confirmDi });
      dialogRef.afterClosed().subscribe((result: confirmDialogAction) => {
        if (result.dialogFeatureName === 'INVOICE_EMAIL_CONFIRM' && result.btnConfirm) {
          this.updateEmail(mailInvoicePayload);
        }
      });
    } else {
      let message: string = 'Invoice has been sent to your mail';
      this.errMsg.showSuccessMessage(message);
    }
  }

  private updateEmail(updateMail) {
    const emailToUpdate = {
      email: updateMail.email
    };
    this.history.updateUserEmail(emailToUpdate).subscribe((res) => {
      if (res.status && res.response) {
        this.updateSessionStoage(res.response.email);
      }
    });
  }

  private updateSessionStoage(newemail) {
    const data = AppAuthenticationService.getUserInfo();
    data.email = newemail;
    AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(data));
  }

  public cancelTrip(bookingData) {
    const cancelPayload = { bookingId: bookingData._id };
    let msgToShow = null;
    if (bookingData.paymentMethod === 'CARD' && bookingData.paymentStatus === 'successful') {
      msgToShow = 'Your cancellation request has been accepted, we initiated your refund you will get in next 8 to 10 working days';
    }

    let infoText = 'Are you sure you want to cancel the trip.';
    const timeDiff = Utils.isTimeDiffMoreThenThreeHours(bookingData.bookingTime);
    if (bookingData.bookingType === 'BOOK_LATER' && timeDiff || bookingData.bookingType === 'BOOK_NOW') {
      infoText = 'Are you sure you want to cancel the trip, cancellation charges S$50 will be apply.'
    }

    let confirmDi: confirmDialog = {
      dialogTitle: 'Cancel',
      dialogContent: infoText,
      dialogPrimaryBtnText: 'Yes',
      dialogSecondaryBtnText: 'No',
      dialogFeatureName: 'CANCEL_TRIP_CONFIRM'
    };
    const dialogRef = this.dialog.open(
      ConfirmModalComponent, { ...this.modalConfig, data: confirmDi });
    dialogRef.afterClosed().subscribe((result: confirmDialogAction) => {
      if (result.dialogFeatureName === 'CANCEL_TRIP_CONFIRM' && result.btnConfirm) {
        this.cancelTripCall(cancelPayload, msgToShow);
      }
    });
  }

  private cancelTripCall(tripCancel, successMsg) {
    this.history.cancelTrip(tripCancel).subscribe((res) => {
      if (res && res.status) {
        this.errMsg.showSuccessMessage(successMsg);
        this.getScheduledTrip();
      }
    });
  }
}
