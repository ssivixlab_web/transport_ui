import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TACommonModule } from 'src/app/common/common.module';
import { HistoryComponent } from './history.component';
import { TAHistoryService } from './history.service';
import { BookingDetailModalComponent } from './modal/booking-detail.modal.component';
import { InvoiceModalComponent } from './modal/Invoice.modal.component';
const historyRoutes: Routes = [
  { path: '', component: HistoryComponent }
];

let Modalcomponets = [
  BookingDetailModalComponent,
  InvoiceModalComponent,
];
@NgModule({
  declarations: [HistoryComponent, ...Modalcomponets],
  imports: [
    TACommonModule,
    RouterModule.forChild(historyRoutes),
  ],
  entryComponents: [...Modalcomponets],
  providers: [TAHistoryService]
})
export class HistoryModule { }
