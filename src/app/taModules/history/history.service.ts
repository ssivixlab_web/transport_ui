import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/common/contants';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable()
export class TAHistoryService {
    constructor(private httpApi: AppHttpApiService) {
    }
    public completedTrip(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.COMPLETED_TRIP}`, payload);
    }
    public scheduledTrip(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.SCHEDULED_TRIP}`, payload);
    }
    public paymentTrip(payload) {
        return this.httpApi.postData(`${API_URL.PAY_TRIP_REQUEST}`, payload);
    }
    public mailInvoice(payload) {
        return this.httpApi.postData(`${API_URL.INVOICE}`, payload);
    }
    public updateUserEmail(payload) {
        return this.httpApi.postData(`${API_URL.EMAIL_UPDATE}`, payload);
    }
    public cancelTrip(cancelTripPayload) {
        return this.httpApi.postData(`${API_URL.CANCEL_TRIP}`, cancelTripPayload);
    }
}
