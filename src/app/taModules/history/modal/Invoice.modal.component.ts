import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    template: `
    <h2 class="mb-0" mat-dialog-title>Invoice</h2>
    <div mat-dialog-content>
    <mat-form-field appearance="outline">
        <mat-label>Email for Invoice</mat-label>
        <input matInput type="email" name="email"  [formControl]="email" >
    </mat-form-field>
    </div>
    <div mat-dialog-actions>
        <button mat-button mat-dialog-close (click)="clickEvent(true)">Send</button>
        <button mat-button mat-dialog-close (click)="clickEvent(false)">Cancel</button>
    </div>
    `,
})
export class InvoiceModalComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<InvoiceModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }
    email: FormControl = new FormControl('');
    ngOnInit() {
        this.email.setValue(this.data.email);
    }

    clickEvent(proceed: boolean) {
        const payload = {
            send: proceed,
            data: { ...this.data, email: this.email.value }
        };
        this.dialogRef.close(payload);
    }
}
