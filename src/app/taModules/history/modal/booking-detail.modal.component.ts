import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
@Component({
    template: `
    <h2 mat-dialog-title class="font-weight-light">Booking Details</h2>
    <mat-dialog-content class="bgWrapp">
    <div class='bgImg'></div>
    <div class="bringfront">
        <div class="d-flex mb-3">
            <div class="leftSection">Date</div>
            <div class="valueSection">{{dateTimeFormat(data.bookingTime)}}</div>
        </div>

        <div class="d-flex mb-3" *ngIf="data.tripType === 'ROUND'">
            <div class="leftSection">Return Time</div>
            <div class="valueSection">{{dateTimeFormat(data.returnTime)}}</div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Pickup Location</div>
            <div class="valueSection">{{data.pickupLocation}}</div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Drop Location</div>
            <div class="valueSection">{{data.dropLocation}}</div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Unit No</div>
            <div class="valueSection">{{data.unitNumber ? data.unitNumber : 'NA' }}</div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Vehicle</div>
            <div class="valueSection"> {{data.vehicleType ==='AMBULANCE' ? 'Medical Transport' : 'Wheelchair fleet'}} </div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Trip</div>
            <div class="valueSection"> {{data.tripType === 'SINGLE' ? 'One Way' : 'Two Way'}} </div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Fare</div>
            <div class="valueSection"> {{data.tripAmount}}  </div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Payment By</div>
            <div class="valueSection"> {{data.paymentMethod ==='CASH' ? 'Cash' : 'Card'}} </div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Additional items</div>
            <div class="valueSection">
            <span class="mr-3" *ngFor="let item of data.additionalItem">
                <mat-checkbox [(ngModel)]="item.isItemUsed" [ngModelOptions]="{standalone: true}"
                [disabled]="true">{{item.itemName}}</mat-checkbox>
            </span>
            </div>
        </div>

        <div class="d-flex mb-3">
            <div class="leftSection">Note</div>
            <div class="valueSection"> {{data.specialNotes}} </div>
        </div>
    </div>
    </mat-dialog-content>
    <mat-dialog-actions align="end">
        <button mat-raised-button [mat-dialog-close]="true" cdkFocusInitial>Close</button>
    </mat-dialog-actions>
    `,
    styles: [`
        .bgWrapp{
            position: relative;
        }
        .bringfront {
            z-index:1;
            position: relative;
        }
        .bgImg{
            background-image: url('../assets/images/transport-img.png');
            width: 100%;
            height: 100%;
            position: absolute;
            background-repeat: no-repeat;
            background-position: center;
            opacity: 0.2;
            left: 0;
            top: 0;
        }
        .valueSection {
            width:100%;
        }
        .leftSection {
            width: 140px;
            flex: 0 0 140px;
            position: relative;
            color: #333;
            font-weight: 500;
        }
        .leftSection::after {
            content: " : ";
            position: absolute;
            right: 10px;
        }
    `]

})
export class BookingDetailModalComponent {
    constructor(public dialogRef: MatDialogRef<BookingDetailModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    dateTimeFormat(bookingTime: string): string {
        if (bookingTime === 'NOT_SURE') {
            return 'Not Sure';
        } else {
            return moment(bookingTime).format('MMM Do, YYYY hh:mm A');
        }
    }

}
