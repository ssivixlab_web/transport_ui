import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TAHomeComponent implements OnInit {
  showbg: boolean = false;
  constructor(
    private router: Router) {
  }

  ngOnInit() {
    this.bg();
    if (AppAuthenticationService.isAuthenticated()) {
      // check if isProfileAvailable
      let otherInfo = AppAuthenticationService.getByKey(SESSION_STORAGE_KEY.OTHER_INFO);
      if (otherInfo.isLoggedIn && !otherInfo.isProfileAvailable) {
        // this.router.navigateByUrl('profileComplete');
      } else {
        this.router.navigateByUrl('ta/book');
      }
    } else {
      this.router.navigateByUrl('');
    }

    this.router.events.subscribe((data) => {
      this.bg();
    });
  }

  private bg() {
    if (this.router.url === '/profileComplete') {
      this.showbg = true;
    } else {
      this.showbg = false;
    }
  }

}
