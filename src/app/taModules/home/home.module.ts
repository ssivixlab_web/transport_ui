import { Platform } from '@angular/cdk/platform';
import { NgModule } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { TACommonModule } from 'src/app/common/common.module';
import { CustomDateAdapter } from 'src/app/common/customDate';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { TAHomeComponent } from './home.component';
import { TAHomeRoutingModule } from './home.routing.module';
import { TAHomeService } from './home.service';
import { TALoginComponent } from './login/login.component';
import { TAMobileComponent } from './mobile/mobile.component';
import { TAOtpComponent } from './otp/otp.component';
import { TAProfileCcompleteComponent } from './profile-complete/profile-complete.component';


@NgModule({
  declarations: [
    TAHomeComponent,
    TALoginComponent,
    TAMobileComponent,
    TAOtpComponent,
    TAProfileCcompleteComponent
  ],
  imports: [
    TAHomeRoutingModule,
    TACommonModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule
  ],
  providers: [
    AppAuthenticationService,
    TAHomeService,
    {
      provide: DateAdapter,
      useClass: CustomDateAdapter,
      deps: [MAT_DATE_LOCALE, Platform]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'DD-MM-YYYY',
        },
        display: {
          dateInput: 'DD-MM-YYYY',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM-YYYY',
        }
      }
    }
  ]
})
export class TAHomeModule { }
