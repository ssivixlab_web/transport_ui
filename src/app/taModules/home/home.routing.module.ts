import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TAHomeComponent } from './home.component';
import { TALoginComponent } from './login/login.component';
import { TAProfileCcompleteComponent } from './profile-complete/profile-complete.component';

export const TAhomeRoutes: Routes = [{
  path: '',
  component: TAHomeComponent,
  children: [
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'
    },
    {
      path: 'login',
      component: TALoginComponent,
    },
    {
      path: 'profileComplete',
      component: TAProfileCcompleteComponent,
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(TAhomeRoutes)],
  exports: [RouterModule]
})
export class TAHomeRoutingModule { }
