import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL, SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';
import { MessagingService } from 'src/app/common/utility/messaging.service';

@Injectable({
    providedIn: 'root',
})
export class TAHomeService {
    constructor(
        private httpApi: AppHttpApiService,
        private messagingService: MessagingService) {
        this.messagingService.getFcmToken();
    }

    // private updatePayloadforNotification(payload: any) {
    //     payload.userFcmToken = this.messagingService.getFcmToken();
    //     return payload;
    // }

    public setSession(user) {
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.TOKEN, user.session);
        AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(user));
    }


    public setSessionData(key: string, value: any) {
        AppAuthenticationService.setSessionStorage(key, JSON.stringify(value));
    }

    mobileNumber(payload): Observable<any> {
        return this.httpApi.postData(API_URL.PHONELOGIN, payload);
    }

    otpVerify(payload): Observable<any> {
        return this.httpApi.postData(API_URL.OTP_VERIFY, payload);
    }

    profileComplete(payload): Observable<any> {
        return this.httpApi.postData(API_URL.USER_UPDATE, payload);
    }

    resendOTP(payload): Observable<any> {
        return this.httpApi.postData(API_URL.RESEND_OTP, payload);
    }

}
