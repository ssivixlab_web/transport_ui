import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { TAAppErrorService } from 'src/app/common/utility/app-error.service';
import { MessagingService } from 'src/app/common/utility/messaging.service';
import { TAHomeService } from '../home.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TALoginComponent implements OnInit {
  loginForm: FormGroup;
  errors: string = '';
  showotp: boolean = false;
  constructor(
    private homeService: TAHomeService,
    private messagingService: MessagingService,
    private errMsg: TAAppErrorService,
    private router: Router) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      mobileNumber: new FormControl('', Validators.required)
    });
  }

  phoneNumber(payload): void {
    let data = {
      countryCode: payload.mobileNumber.dialCode,
      phoneNumber: payload.mobileNumber.number.replace(/\s/g, '')
    };
    this.homeService.mobileNumber(data)
      .subscribe({
        next: (res: boolean) => {
          if (res) {
            this.showOTPandAddControl();
          }
        },
        error: (err: any) => {
          this.errors = err.error_message;
        },
        complete: () => {

        }
      });
  }

  showOTPandAddControl() {
    this.showotp = true;
    this.loginForm.addControl('OTP', new FormControl('', [Validators.required, Validators.minLength(4)]));
  }

  otp(payload) {
    this.messagingService.token$.subscribe((res) => {
      if (res && res !== null) {
        const otpDetails = {
          countryCode: payload.mobileNumber.dialCode,
          phoneNumber: payload.mobileNumber.number.replace(/\s/g, ''),
          OTP: payload.OTP,
          OSType: 'WEB',
          fcmToken: res
        };
        this.login(otpDetails);
      } else {
        // user has not enabled the FCM.
        this.errMsg.showApplicationError('FCM');
      }
    });
  }

  login(data) {
    this.homeService.otpVerify(data)
      .subscribe({
        next: (res) => {
          if (res.saved) {
            this.homeService.setSessionData(SESSION_STORAGE_KEY.TOKEN, res.session);
            this.homeService.setSessionData(SESSION_STORAGE_KEY.USER_DATA, res.user);
            this.homeService.setSessionData(SESSION_STORAGE_KEY.OTHER_INFO, {
              isLoggedIn: true,
              isProfileAvailable: res.isProfileAvailable
            });
            if (res.isProfileAvailable) {
              this.router.navigateByUrl('ta/book');
            } else {
              this.router.navigateByUrl('profileComplete');
            }
          } else {
            alert('invalid OTP')
          }
        },
        error: (err: any) => {
          this.errors = err.error_message;
        },
        complete: () => {

        }
      });
  }

  hideOTP($ev) {
    this.loginForm.removeControl('OTP');
    this.loginForm.updateValueAndValidity();
    this.showotp = false;
  }

}
