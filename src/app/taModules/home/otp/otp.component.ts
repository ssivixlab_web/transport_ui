import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TAHomeService } from '../home.service';

@Component({
  selector: 'ta-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class TAOtpComponent implements OnInit {
  @Input() fg: FormGroup;
  @Output() backEv = new EventEmitter();
  constructor(private homeService: TAHomeService) {
  }

  ngOnInit() {
  }

  back() {
    this.backEv.emit('hide');
  }

  resendOTP(val) {
    let userDetail = {
      countryCode: val.mobileNumber.dialCode,
      phoneNumber: val.mobileNumber.number.replace(/\s/g, '')
    };
    this.homeService.resendOTP(userDetail).subscribe((res) => {
      if (res.status) { alert('OTP sent again successfully') }
    });
  }

}
