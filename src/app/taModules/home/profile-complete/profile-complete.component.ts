import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { TAHomeService } from '../home.service';

@Component({
  templateUrl: './profile-complete.component.html',
  styleUrls: ['./profile-complete.component.scss']
})
export class TAProfileCcompleteComponent implements OnInit {
  profileForm: FormGroup;
  constructor(
    private homeService: TAHomeService,
    private fb: FormBuilder,
    private router: Router) {
  }

  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      gender: ['', Validators.required],
      email: ['', Validators.required],
      referralCode: [],
    });
  }

  submit(val) {
    let newPayload = { ...val };
    newPayload.dateOfBirth = moment(newPayload.dateOfBirth).format('YYYY-MM-DD');
    this.homeService.profileComplete(newPayload).subscribe((res) => {
      console.log('res', res)
      this.setSesion(res);
      //                await AsyncStorage.setItem(AppStrings.contracts.LOGGED_IN_USER_DETAILS, JSON.stringify(response.data.response));
      //                await AsyncStorage.setItem(AppStrings.contracts.IS_PROFILE_AVAILABLE, JSON.stringify({isProfileAvailable: true}));

    })

  }

  setSesion(res) {
    this.homeService.setSessionData(SESSION_STORAGE_KEY.OTHER_INFO, {
      isLoggedIn: true,
      isProfileAvailable: true
    });
    this.homeService.setSessionData(SESSION_STORAGE_KEY.USER_DATA, res.response);
    this.router.navigateByUrl('ta/profile/edit');
  }

}
