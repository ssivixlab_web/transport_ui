import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TACommonModule } from 'src/app/common/common.module';
import { NotificationComponent } from './notification.component';

const historyRoutes: Routes = [
  { path: '', component: NotificationComponent }
];

@NgModule({
  declarations: [NotificationComponent],
  imports: [
    TACommonModule,
    RouterModule.forChild(historyRoutes),
  ]
})
export class NotificationModule { }
