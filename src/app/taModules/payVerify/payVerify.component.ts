import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { TAPayVerifyService } from './payVerify.service';

@Component({
    templateUrl: './payVerify.component.html',
})
export class PayVerifyComponent implements OnInit {
    counter = 8;
    successTransition = false;
    errorTransition = false;
    constructor(private route: Router, private payVerifyService: TAPayVerifyService) { }
    ngOnInit() {
        this.checkPaymentStatus();
    }

    private checkPaymentStatus() {
        if (sessionStorage.getItem(SESSION_STORAGE_KEY.PAYMENT) !== '') {
            const payload = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.PAYMENT));
            this.payVerifyService.paymentStatusCheck(payload).subscribe((paystatus) => {
                if (paystatus.status && paystatus.data === 'Success') {
                    this.successTransition = true;
                    this.startTimertoCloseWindow();
                } else {
                    this.errorTransition = true;
                }
            }, (errStatus) => {
                this.errorTransition = true;
            });
        } else {
            this.errorTransition = true;
        }
    }

    private startTimertoCloseWindow() {
        sessionStorage.removeItem(SESSION_STORAGE_KEY.PAYMENT);
        timer(0, 1000).subscribe(() => {
            if (this.counter > 0) {
                this.counter--;
            } else if (this.counter === 0) {
                window.close();
                this.route.navigate(['ta/history']);
                this.counter--;
            }
        });
    }
}
