import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TACommonModule } from 'src/app/common/common.module';
import { PayVerifyComponent } from './payVerify.component';
import { TAPayVerifyService } from './payVerify.service';
const historyRoutes: Routes = [
    { path: '', component: PayVerifyComponent }
];

@NgModule({
    declarations: [PayVerifyComponent],
    imports: [
        TACommonModule,
        RouterModule.forChild(historyRoutes),
    ],
    providers: [TAPayVerifyService]
})
export class PayVerifyModule { }
