import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/app/common/contants';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable()
export class TAPayVerifyService {
    constructor(private httpApi: AppHttpApiService) {
    }
    public paymentStatusCheck(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.PAYMENT_STATUS_VERIFY}`, payload);
    }
}
