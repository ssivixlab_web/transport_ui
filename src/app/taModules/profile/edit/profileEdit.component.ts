import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { SELF_PROFILE, SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { IInsurnaceList, IKeyVal } from 'src/app/common/utility/api-data.model';
import { TAAppErrorService } from 'src/app/common/utility/app-error.service';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { TAProfileService, UserObj } from '../profile.service';

@Component({
  templateUrl: './profileEdit.component.html',
  styleUrls: ['./profileEdit.component.scss']
})
export class TAProfileEditComponent implements OnInit {
  private relationItems: IKeyVal[] = [
    { key: 'self', val: 'Self' },
    { key: 'spouse', val: 'Spouse' },
    { key: 'son', val: 'Son' },
    { key: 'daughter', val: 'Daughter' },
    { key: 'others', val: 'Others' }
  ];
  insuranceData: IInsurnaceList;
  isInsuranceToogleOn: boolean = false;
  profileForm: FormGroup;
  hideEmailInView: boolean = false;
  isNewProfileAdd: boolean = false;
  // Element bind in UI
  memberRelationShip: IKeyVal[];

  tempImgpath: ArrayBuffer | string;
  files: any;


  constructor(
    private profileService: TAProfileService,
    private activateRoute: ActivatedRoute,
    private errMsg: TAAppErrorService,
    private router: Router,
    private fb: FormBuilder) {
  }

  private createAForm() {
    this.profileForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [],
      dateOfBirth: [],
      gender: [],
      relation: [],
      relationText: [],
      insuranceProvider: [],
      insuranceNumber: [],
      /// -self-
      email: [],
      NRIC: [],
      relativeId: []
    });
    this.profileForm.reset();
  }

  private selfUser() {
    // keep only 'self'
    this.memberRelationShip = this.relationItems.filter(item => item.key === SELF_PROFILE);
    this.profileForm.get('relation').setValue(SELF_PROFILE);
    this.profileForm.get('relation').disable();
  }

  private forFamilyMember() {
    // remove 'self'
    this.memberRelationShip = this.relationItems.filter(item => item.key !== SELF_PROFILE);
    this.hideEmailInView = true;
    // this.profileForm.removeControl('email');
  }

  private getListOfInsuance() {
    this.profileService.getListOfInsurance().subscribe((res: IInsurnaceList) => {
      this.insuranceData = res;
    });
  }
  private isFirstTime() {
    let userData = AppAuthenticationService.getUserInfo();
    if (!userData.isProfileAvailable) {
      // this.selfUser(); TODO
    }
  }

  ngOnInit() {
    this.getListOfInsuance();
    this.createAForm();
    this.isFirstTime();
    this.activateRoute.paramMap.pipe(map(() => window.history.state)).subscribe((res) => {
      if (!res.action) {
        this.router.navigateByUrl('ta/profile'); return;
      } else {
        this.isNewProfileAdd = res.action.isNewAdd;
        if (res.action.isNewAdd) {
          this.profileForm.removeControl('relativeId');
          this.forFamilyMember();
        } else {
          this.fetchUserProfile(res.action.user);
        }
      }
    });
  }

  private prepareUser(res, user: UserObj) {
    let userData = res.relativeDetails[0];
    let udata = new UserObj();
    if (res.relativeDetails[0].spouse === SELF_PROFILE) {
      this.selfUser();
      udata.email = user.email;
    } else {
      udata.email = '';
      this.forFamilyMember();
    }

    if (res.relativeDetails[0].insuranceNumber !== null) {
      this.isInsuranceToogleOn = true;
    }

    udata.firstName = userData.firstName;
    udata.lastName = userData.lastName;
    udata.dateOfBirth = userData.dateOfBirth;
    udata.gender = userData.gender;
    udata.relation = userData.spouse;
    udata.relationText = userData.spouse;
    udata.insuranceNumber = (!userData.insuranceNumber) ? null : userData.insuranceNumber;
    udata.insuranceProvider = (!userData.insuranceProvider) ? null : userData.insuranceProvider;
    // udata.email = (userData.email) ? userData.email : '';
    udata.NRIC = userData.NRIC;
    udata.relativeId = userData._id;
    this.tempImgpath = (userData.profilePic !== 'null') ? userData.profilePic : '';
    this.profileForm.patchValue(udata);
  }

  private fetchUserProfile(user: UserObj) {
    this.profileService.getById(user.relativeId).subscribe((res) => {
      this.prepareUser(res, user);
    });
  }

  onFileChanged(event: any) {
    this.files = event.target.files[0];

    if (this.files.length === 0) {
      return;
    }

    const mimeType = this.files.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.files);
    reader.onload = () => {
      this.tempImgpath = reader.result;
      this.uploadPic(this.files);
    };
  }

  uploadPic(file) {
    const form = new FormData();
    form.append('image', file, file.name);

    if (this.profileForm.get('relation').value !== SELF_PROFILE) {
      let rid = this.profileForm.get('relativeId') ? this.profileForm.get('relativeId').value : '';
      this.profileService.uploadRelativeProfilePic(form, rid).subscribe((res) => { })
    } else {
      this.profileService.uploadProfilePic(form).subscribe((res) => { })
    }
    // 
  }

  private payloadCheck(payload) {
    let temp = { ...payload };
    temp.relationText = temp.relation;
    temp.dateOfBirth = moment(temp.dateOfBirth, 'YYYY-MM-DD').toDate().toISOString();
    if (!this.isInsuranceToogleOn) {
      temp.insuranceNumber = null;
      temp.insuranceProvider = null;
    }
    return temp;
  }

  updateProfile(payload) {
    let newPayload = this.payloadCheck(payload);
    this.profileService.updateUser(newPayload).subscribe((res) => {
      if (res.status) {
        this.errMsg.showSuccessMessage('Profile Updated successfully');
        this.updateSession();
        this.router.navigateByUrl('ta/profile');
      }
    });
  }

  addUser(payload) {
    let newPayload = this.payloadCheck(payload);
    this.profileService.addUser(newPayload).subscribe((res) => {
      if (res.status) {
        this.errMsg.showSuccessMessage('Profile Added successfully');
        this.router.navigateByUrl('ta/profile');
      }
    });
  }


  private updateSession() {
    let data = AppAuthenticationService.getUserInfo();
    data.isProfileAvailable = true;
    AppAuthenticationService.setSessionStorage(SESSION_STORAGE_KEY.USER_DATA, JSON.stringify(data));
  }
}
