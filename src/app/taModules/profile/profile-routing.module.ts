import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TAProfileEditComponent } from './edit/profileEdit.component';
import { TAProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    component: TAProfileComponent,
    // canActivate: [profileCheck],
  },
  {
    path: 'edit',
    component: TAProfileEditComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class TAProfileRoutingModule {
}
