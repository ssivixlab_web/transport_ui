import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { placeholderImg } from 'src/app/common/contants';
import { Utils } from 'src/app/common/utility/utility';
import { TAProfileService, UserObj } from './profile.service';


@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class TAProfileComponent implements OnInit {

  userList = new BehaviorSubject<any[]>([]);

  constructor(
    private profileService: TAProfileService,
    private router: Router) { }

  private getUserObj() {
    return new UserObj();
  }
  ngOnInit() {

    this.profileService.getUserProfile().subscribe((res) => {
      // if (res.userDetails.length >= 0) {
      //   this.assignMobileAndId(res);
      // } else {
      let userData = res.userDetails[0];
      // await AsyncStorage.setItem(AppStrings.contracts.LOGGED_IN_USER_DETAILS, JSON.stringify(stat.userDetails[0]));
      if (userData.relativeDetails) {
        this.addUserProfile(userData);
      }
      this.addFamilyMembers(res.familyMembers);
      // }
    });
  }

  assignMobileAndId(res) {
    let case1 = this.getUserObj();
    case1.mobNumber = res.userDetails[0].phoneNumber;
    // case1.relatives = res.familyMembers;
    case1.relativeId = res.userDetails[0]._id;
    const a = [...this.userList.value, case1];
    this.userList.next(a);
  }

  addUserProfile(userData) {
    let udata = this.getUserObj();
    udata.profilePic = userData.profilePic !== null ? userData.profilePic : placeholderImg;
    udata.firstName = userData.relativeDetails.firstName;
    udata.lastName = userData.relativeDetails.lastName;
    udata.dateOfBirth = userData.relativeDetails.dateOfBirth;
    udata.ageUI = `${Utils.getAgeFromDateOfBirth(userData.relativeDetails.dateOfBirth)} Yrs`;
    udata.gender = userData.relativeDetails.gender;
    udata.mobNumber = userData.phoneNumber;
    udata.email = (userData.email) ? userData.email : '';
    udata.NRIC = userData.relativeDetails.NRIC;
    udata.insuranceNumber = (!userData.relativeDetails.insuranceNumber) ? null : userData.relativeDetails.insuranceNumber;
    udata.insuranceProvider = (!userData.relativeDetails.insuranceProvider) ? null : userData.relativeDetails.insuranceProvider;
    udata.relativeId = userData.relativeDetails._id;
    udata.relation = 'self';
    udata.relationText = 'self';
    const a = [...this.userList.value, udata];
    this.userList.next(a);
  }

  addFamilyMembers(res) {
    res.forEach(element => {
      let udata = this.getUserObj();
      udata.profilePic = element.relativeDetails.profilePic !== null && element.relativeDetails.profilePic !== 'null' ?
        element.relativeDetails.profilePic : placeholderImg;
      udata.firstName = element.relativeDetails.firstName;
      udata.lastName = element.relativeDetails.lastName;
      udata.relation = element.relativeDetails.spouse;
      udata.relationText = element.relativeDetails.spouse;
      udata.insuranceNumber = (!element.relativeDetails.insuranceNumber) ? null : element.relativeDetails.insuranceNumber;
      udata.insuranceProvider = (!element.relativeDetails.insuranceProvider) ? null : element.relativeDetails.insuranceProvider;
      udata.relativeId = element.relativeDetails._id;
      udata.email = '';
      const a = [...this.userList.value, udata];
      this.userList.next(a);
    });
  }

  buttonClick(isnew: boolean, userObj: UserObj = null) {
    let data = {
      isNewAdd: isnew,
      user: userObj
    };
    this.router.navigateByUrl('ta/profile/edit', { state: { action: data } });
  }

}
