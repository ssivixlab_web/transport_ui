import { Platform } from '@angular/cdk/platform';
import { NgModule } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { TACommonModule } from 'src/app/common/common.module';
import { CustomDateAdapter } from 'src/app/common/customDate';
import { TAProfileEditComponent } from './edit/profileEdit.component';
import { TAProfileRoutingModule } from './profile-routing.module';
import { TAProfileComponent } from './profile.component';
import { TAProfileService } from './profile.service';

@NgModule({
  declarations: [TAProfileComponent, TAProfileEditComponent],
  imports: [
    TACommonModule,
    TAProfileRoutingModule,
  ],
  providers: [TAProfileService,
    {
      provide: DateAdapter,
      useClass: CustomDateAdapter,
      deps: [MAT_DATE_LOCALE, Platform]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'DD-MM-YYYY',
        },
        display: {
          dateInput: 'DD-MM-YYYY',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM-YYYY',
        }
      }
    }
  ]
})
export class TAProfileModule { }
