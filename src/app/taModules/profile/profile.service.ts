import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { API_URL, NA, placeholderImg, STATIC_BASE_API_URL } from 'src/app/common/contants';
import { IInsurnaceList } from 'src/app/common/utility/api-data.model';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable()
export class TAProfileService {

    private _cacheItems: Observable<Array<Observable<any>>> = new Observable();
    constructor(private httpApi: AppHttpApiService) {
    }
    /**
     * Get List of ListOfInsurance
     */
    public getListOfInsurance(): Observable<IInsurnaceList> {
        if (!(this._cacheItems && this._cacheItems[`${STATIC_BASE_API_URL.INSURANCE_PROVIDER}`])) {
            this._cacheItems[`${STATIC_BASE_API_URL.INSURANCE_PROVIDER}`] =
                this.httpApi.getData(`${STATIC_BASE_API_URL.INSURANCE_PROVIDER}`)
                    .pipe(
                        map((res) => {
                            return res.status;
                        }),
                        publishReplay(1),
                        refCount()
                    );
        }
        return this._cacheItems[`${STATIC_BASE_API_URL.INSURANCE_PROVIDER}`];
    }

    updateUser(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.EDIT_USER}`, payload);
    }

    addUser(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.ADD_RELATIVE}`, payload);
    }

    getUserProfile(): Observable<any> {
        return this.httpApi.postData(`${API_URL.USER_PROFILE}`, {});
    }

    getById(id): Observable<any> {
        return this.httpApi.postData(`${API_URL.FETCH_BY_ID}`, { relativeId: id });
    }

    uploadProfilePic(payload) {
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        return this.httpApi.postData(`${API_URL.UPLOAD_PROFILE_PIC}`, payload, HttpUploadOptions.headers);
    }

    uploadRelativeProfilePic(payload, relativeId) {
        const HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        return this.httpApi.postData(`${API_URL.UPLOAD_RELATIVE_PIC}?relativeId=${relativeId}`, payload, HttpUploadOptions.headers);
    }
}

export class UserObj {
    constructor(
        public profilePic: string = placeholderImg,
        public firstName: string = NA,
        public lastName: string = NA,
        public dateOfBirth: string = NA,
        public gender: string = NA,
        public relation: string = NA,
        public relationText: string = NA,
        public insuranceProvider: string = null,
        public insuranceNumber: string = null,
        // --- above all are common
        public email: string = NA,
        public NRIC: string = NA,
        public relativeId: string = NA,
        // -- below not
        public mobNumber: string = NA,
        public ageUI: string = NA
    ) {

    }
}
