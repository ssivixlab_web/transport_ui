import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileCheckGuardService implements CanActivate {
  constructor(public router: Router) { }

  canActivate(): boolean {

    if (!AppAuthenticationService.getUserInfo().isProfileAvailable) {
      return false;
    }
    return true;
  }
}
