import { Component, OnInit } from '@angular/core';
import { STATIC_BASE_API_URL } from 'src/app/common/contants';

@Component({
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.scss']
})
export class TATermsConditionComponent implements OnInit {
  ifWrapper;
  constructor() { }

  ngOnInit() {
    const url = STATIC_BASE_API_URL.TERMSCONDITION;
    this.ifWrapper = `<iframe class="w-100 h-100" src="${url}" frameborder="0"></iframe>`;
  }

}
