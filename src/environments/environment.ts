// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endPointUrl: 'http://localhost:3001/',
  // endPointUrl: 'http://18.222.133.72:3000/',
  firebaseConfig: {
    apiKey: 'AIzaSyBCR0ijuciUC93K2M6L7Az0Xr0Jkx-HN70',
    authDomain: 'myclnq-requester-ios.firebaseapp.com',
    databaseURL: 'https://myclnq-requester-ios.firebaseio.com',
    projectId: 'myclnq-requester-ios',
    storageBucket: 'myclnq-requester-ios.appspot.com',
    messagingSenderId: '84286705331',
    appId: '1:84286705331:web:c1380a00ab09c6dbcaaac6',
    measurementId: 'G-J5SKJ0SYS1'
  },
  firebaseConfig1: {
    apiKey: 'AIzaSyCCU7jy1YyWAoQUNHwWkxVaBafSkOMnHNo',
    authDomain: 'myclnq-dc1de.firebaseapp.com',
    databaseURL: 'https://myclnq-dc1de.firebaseio.com',
    projectId: 'myclnq-dc1de',
    storageBucket: 'myclnq-dc1de.appspot.com',
    messagingSenderId: '1031743870650',
    appId: '1:1031743870650:web:2b13615ff7ef9da26f1483',
    measurementId: 'G-R32C5CEYSN'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
