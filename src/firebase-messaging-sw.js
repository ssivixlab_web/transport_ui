importScripts("https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js");

var firebaseConfig = {
  apiKey: "AIzaSyBCR0ijuciUC93K2M6L7Az0Xr0Jkx-HN70",
  authDomain: "myclnq-requester-ios.firebaseapp.com",
  databaseURL: "https://myclnq-requester-ios.firebaseio.com",
  projectId: "myclnq-requester-ios",
  storageBucket: "myclnq-requester-ios.appspot.com",
  messagingSenderId: "84286705331",
  appId: "1:84286705331:web:c1380a00ab09c6dbcaaac6",
  measurementId: "G-J5SKJ0SYS1",
};
firebase.initializeApp(firebaseConfig);

var firebaseConfig1 = {
  apiKey: "AIzaSyCCU7jy1YyWAoQUNHwWkxVaBafSkOMnHNo",
  authDomain: "myclnq-dc1de.firebaseapp.com",
  databaseURL: "https://myclnq-dc1de.firebaseio.com",
  projectId: "myclnq-dc1de",
  storageBucket: "myclnq-dc1de.appspot.com",
  messagingSenderId: "1031743870650",
  appId: "1:1031743870650:web:2b13615ff7ef9da26f1483",
  measurementId: "G-R32C5CEYSN",
};
firebase.initializeApp(firebaseConfig1, "Secondary");

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// messaging.onMessage((payload) => {

// });
messaging.setBackgroundMessageHandler(function (payload) {
  // Customize notification here
  const notificationTitle = "Background Message Title";
  const notificationOptions = {
    body: "Background Message body.",
    icon: "/firebase-logo.png",
  };

  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
